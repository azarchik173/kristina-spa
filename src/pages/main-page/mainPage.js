import React from "react";
import { Clients } from "./components/Clients";
import { Footer } from "./components/Footer";
import { GeneralServices } from "./components/GeneralServices";
import { Skills } from "./components/Skills";
import { Title } from "./components/Title";

import "./mainPage.scss";

export const MainPage = () => {
  return (
    <div className="main-container">
      <Title />
      <GeneralServices />
      <Clients />
      <Skills />
      <Footer />
    </div>
  );
};
