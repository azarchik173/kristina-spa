import React from "react";
import "./Title.scss";

export const Title = () => {
  return (
    <header className="title-containter">
      <span className="name">КРИСТИНА ИЛЬИНСКАЯ</span>
      <span className="divider" />
      <span className="specific-name">Эксперт по рекламе и СО</span>
    </header>
  );
};
