import React from "react";

import "./index.scss";

const servicesArray = [
  {
    titleName: "БРЕНДИНГ",
    description:
      "аналитика и разработка платфоомы Бренда, создание элементов вербальной и визуальнои коммуникации",
  },
  {
    titleName: "WEB РАЗРАБОТКА",
    description:
      "промо страницы, корпоративные саиты, интернет-магазины, торговые площадки, сложные проекты.",
  },
  {
    titleName: "ИНТЕРНЕТ-МАРКЕТИНГ",
    description:
      "комплексное продвижение, контекстная и медийная реклама, поисковая оптимизация, маркетинг в соцсетях, работа с репутацией, контент-маркетинг, ета-маркетинг, аналитика и нестандартные решения",
  },
  {
    titleName: "РАЗРАБОТКА DIGITAL-СТРАТЕГИЙ",
    description: "",
  },
  {
    titleName:
      "ПРОИЗВОДСТВО И ПОДДЕРЖКА МЕДИЙНЫХ РЕКЛАМНЫХ КАМПАНИЙ, РАЗРАБОТКА БАННЕРОВ",
    description: "",
  },
];

export const GeneralServices = () => {
  return (
    <div className="services-container">
      <span className="main-text">Виды услуг</span>
      {servicesArray.map((value) => (
        <span className="service" key={value.titleName}>
          <span>•</span> <span>{value.titleName}</span>{" "}
          {value.description && `- ${value.description}`}
        </span>
      ))}
    </div>
  );
};
