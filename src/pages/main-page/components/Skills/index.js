import React from "react";

import "./index.scss";

const skillArray = [
  "Работа в Excel",
  "Тайм-менеджмент",
  "Знание основ юзабилити ",
  "Умение писать продающие заголовки и тексты объявлений",
  "Понимание психологии маркетинга",
  "Цифровая иллюстрация",
  "Понимание принципов работы систем контекстной рекламы",
  "Каллиграфия",
  "Графический дизайн",
  "Визуализация данных",
];

export const Skills = () => {
  return (
    <div className="skills-container">
      <span className="main-text">Навыки</span>
      {skillArray.map((value) => (
        <span className="skill" key={value.titleName}>
          <span>•</span> <span>{value}</span>
        </span>
      ))}
    </div>
  );
};
