import React, { useState } from "react";
import ImageGallery from "react-image-gallery";

import AbsolutIcon from "../../../../assets/img/compains/absolute.jpg";
import AfimalIcon from "../../../../assets/img/compains/afimall.jpg";
import AlfaIcon from "../../../../assets/img/compains/alfa.svg";
import BeelineIcon from "../../../../assets/img/compains/beeline.png";
import CvetnoyIcon from "../../../../assets/img/compains/Cventoy.jpg";
import EdimIcon from "../../../../assets/img/compains/edim.jpg";
import IlushinIcon from "../../../../assets/img/compains/Ilyushin.png";
import IntouristIcon from "../../../../assets/img/compains/intourist.jpg";
import RossetiIcon from "../../../../assets/img/compains/rosseti.png";
import RusatomIcon from "../../../../assets/img/compains/rusatom.png";
import SiburIcon from "../../../../assets/img/compains/Sibur_logo.png";
import StockmanIcon from "../../../../assets/img/compains/Stockmann.png";
import TvoeIcon from "../../../../assets/img/compains/tvoe.jpg";

import "./index.scss";

const images = [
  {
    title: "Абсолют страхавание",
    original: AbsolutIcon,
  },
  {
    title: "Афимолл сити",
    original: AfimalIcon,
  },
  {
    title: "Альфа банк",

    original: AlfaIcon,
  },
  {
    title: "Билайн",
    original: BeelineIcon,
  },
  {
    title: 'Универмаг "Цветной"',
    original: CvetnoyIcon,
  },
  {
    title: "Едим Дома",
    original: EdimIcon,
  },
  {
    title: 'КБ "Илюшин"',
    original: IlushinIcon,
  },
  {
    title: "Интурист",
    original: IntouristIcon,
  },
  {
    title: "Россети",
    original: RossetiIcon,
  },
  {
    title: "Росатом",
    original: RusatomIcon,
  },
  {
    title: "Сибур",
    original: SiburIcon,
  },
  {
    title: "Stockmann",
    original: StockmanIcon,
  },
  {
    title: "ТВОЕ",
    original: TvoeIcon,
  },
].map((value) => ({ ...value, originalHeight: 200, originalWeight: 200 }));

export const Clients = () => {
  const [index, setIndex] = useState(0);
  const onSlide = (data) => {
    setIndex(data);
  };
  return (
    <div className="clients-container">
      <span className="title">Клиенты</span>
      <div className="gallery">
        <ImageGallery
          onSlide={onSlide}
          items={images}
          autoPlay={true}
          slideDuration={500}
          showFullscreenButton={false}
          showPlayButton={false}
          showThumbnails={false}
          showNav={false}
        />
        <span className="brand-name">{images[index].title}</span>
      </div>
    </div>
  );
};
