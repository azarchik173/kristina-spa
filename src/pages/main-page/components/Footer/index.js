import React from "react";
import avatarImage from "../../../../assets/img/avatar.jpg";
import buissnesIcon from "../../../../assets/img/buissnes.png";
import girlIcon from "../../../../assets/img/girl.png";

import "./index.scss";

const VerticalDivider = () => (
  <div
    style={{
      width: "2px",
      background: "rgb(1 1 1 / 25%)",
      margin: "0 30px",
    }}
  />
);

export const Footer = () => {
  return (
    <div className="footer-container">
      <div className="contact-container">
        <img src={avatarImage} className="avatar-container" />
        <div className="contact">
          <span className="contact__details">
            <img className="icon" src={buissnesIcon} />
            <a href="mailto:skaredova.ti@gmail.com">skaredova.ti@gmail.com</a>
          </span>
          <span className="contact__details">
            <img className="icon" src={girlIcon} />
            <a href="tel:+7(962)634-34-62">+7 (962) 634-34-62</a>
          </span>
        </div>
      </div>
      <VerticalDivider />
      <div className="education">
        <span className="education__header">Образование</span>
        <div>
          <span>Сочинский Государственный университет</span>
          <span>Реклама и связи с общественностью</span>
          <span>2018-2023</span>
        </div>
      </div>
    </div>
  );
};
